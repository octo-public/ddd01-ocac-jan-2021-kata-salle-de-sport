export class SouscrireAbonnementCommand {
	constructor(readonly formuleId: string, readonly periode: 'mensuelle' | 'annuelle', readonly membreId: string) {
	}
}
