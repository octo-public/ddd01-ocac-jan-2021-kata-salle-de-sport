import { AbonnementSouscritParMembreEvent } from '../domain/abonnement/abonnement-souscrit-par-membre-event'
import { NotificationSender }               from '../domain/notification-sender'

export class AbonnementSouscritListener {
	constructor(private notificationSender: NotificationSender) {
	}

	async handle(event: AbonnementSouscritParMembreEvent) {
		await this.notificationSender.send('email@yopmail.com')
	}
}
