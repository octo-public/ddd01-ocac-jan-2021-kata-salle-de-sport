import { Abonnement }                 from '../domain/abonnement/abonnement'
import { FormuleId }                  from '../domain/abonnement/formule-id'
import { MembreId }                   from '../domain/abonnement/membre-id'
import { Montant }                    from '../domain/abonnement/montant'
import { EventPublisher }             from '../domain/event-publisher'
import { FormuleRepository }          from '../domain/repository/formule-repository'
import { MembreRepository }           from '../domain/repository/membre-repository'
import { SouscrireAbonnementCommand } from './souscrire-abonnement-command'

export class SouscrireAbonnementHandler {
	constructor(private eventPublisher: EventPublisher,
							private membreRepository: MembreRepository,
							private formuleRepository: FormuleRepository) {
	}

	async execute(command: SouscrireAbonnementCommand) {
		const typeDeMembre = await this.membreRepository.getTypePour(command.membreId)
		const prixFormule  = await this.formuleRepository.getPrix(command.formuleId)

		const abonnement = Abonnement.souscrire(
			MembreId.of(command.membreId),
			FormuleId.of(command.formuleId),
			new Montant(prixFormule),
			typeDeMembre,
			command.periode
		)

		await this.eventPublisher.publish(abonnement.raisedEvents)
	}
}
