import { TypeMembre } from '../abonnement/type-membre'

export interface MembreRepository {
	getTypePour(id: string): Promise<TypeMembre>
}
