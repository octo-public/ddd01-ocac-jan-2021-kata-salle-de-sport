export interface NotificationSender {
	send(email: string): Promise<void>
}
