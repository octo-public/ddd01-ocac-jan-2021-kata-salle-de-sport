import { AbonnementPeriode }                from './abonnement-periode'
import { AbonnementSouscritParMembreEvent } from './abonnement-souscrit-par-membre-event'
import { DomainEvent }                      from '../domain-event'
import { FormuleId }                        from './formule-id'
import { MembreId }                         from './membre-id'
import { Montant }                          from './montant'
import { TypeMembre }                       from './type-membre'

export class Abonnement {
	private _raisedEvents: DomainEvent[] = []

	static souscrire(membreId: MembreId,
									 formuleId: FormuleId,
									 prixFormule: Montant,
									 typeMembre: TypeMembre,
									 periode: AbonnementPeriode): Abonnement {
		const abonnement = new Abonnement()

		const tarifApplique = typeMembre.appliquerReduction(prixFormule)

		abonnement._raisedEvents.push(new AbonnementSouscritParMembreEvent(
			'abonnement-id',
			membreId.membreId,
			formuleId.formuleId,
			tarifApplique.valeur,
			typeMembre.isEtudiant,
			periode
		))

		return abonnement
	}

	get raisedEvents(): DomainEvent[] {
		return this._raisedEvents
	}
}
