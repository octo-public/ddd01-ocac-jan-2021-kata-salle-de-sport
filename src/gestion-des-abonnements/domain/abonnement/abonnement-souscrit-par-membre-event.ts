import { DomainEvent } from '../domain-event'

export class AbonnementSouscritParMembreEvent implements DomainEvent {
	constructor(readonly abonnementId: string,
							readonly membreId: string,
							readonly formuleId: string,
							readonly tarifApplique: number,
							readonly isEtudiant: boolean,
							readonly mensuel: string) {
	}
}
