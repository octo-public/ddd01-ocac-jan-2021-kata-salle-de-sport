export class FormuleId {
	private constructor(readonly formuleId: string) {
	}

	static of(formuleId: string): FormuleId {
		return new FormuleId(formuleId)
	}
}
