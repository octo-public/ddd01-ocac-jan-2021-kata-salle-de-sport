export class Montant {
	constructor(private _valeur: number) {
	}

	get valeur(): number {
		return this._valeur
	}

	multiplier(number: number): Montant {
		return new Montant(this._valeur * number)
	}
}
