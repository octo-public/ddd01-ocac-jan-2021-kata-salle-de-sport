export class MembreId {
	private constructor(readonly membreId: string) {
	}

	static of(membreId: string): MembreId {
		return new MembreId(membreId)
	}
}
