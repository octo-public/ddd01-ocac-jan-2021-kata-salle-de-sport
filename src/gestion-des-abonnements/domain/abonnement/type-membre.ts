import { Montant } from './montant'

export abstract class TypeMembre {
	abstract get isEtudiant(): boolean

	abstract appliquerReduction(prixFormule: Montant): Montant
}

export class TypeMembreEtudiant extends TypeMembre {
	get isEtudiant(): boolean {
		return true
	}

	appliquerReduction(prixFormule: Montant): Montant {
		return prixFormule.multiplier(0.8)
	}
}

export class TypeMembreStandard extends TypeMembre {
	get isEtudiant(): boolean {
		return false
	}

	appliquerReduction(prixFormule: Montant): Montant {
		return prixFormule
	}
}
