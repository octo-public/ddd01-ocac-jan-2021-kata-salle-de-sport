import chai, { expect }                           from 'chai'
import sinon                                      from 'sinon'
import sinonChai                                  from 'sinon-chai'
import { AbonnementSouscritParMembreEvent }       from '../gestion-des-abonnements/domain/abonnement/abonnement-souscrit-par-membre-event'
import { EventPublisher }                         from '../gestion-des-abonnements/domain/event-publisher'
import { FormuleRepository }                      from '../gestion-des-abonnements/domain/repository/formule-repository'
import { MembreRepository }                       from '../gestion-des-abonnements/domain/repository/membre-repository'
import { TypeMembreEtudiant, TypeMembreStandard } from '../gestion-des-abonnements/domain/abonnement/type-membre'
import { SouscrireAbonnementCommand }             from '../gestion-des-abonnements/use-cases/souscrire-abonnement-command'
import { SouscrireAbonnementHandler }             from '../gestion-des-abonnements/use-cases/souscrire-abonnement-handler'

chai.use(sinonChai)

describe('Souscription à un abonnement', () => {
	let eventPublisher: EventPublisher
	let membreRepository: MembreRepository
	let formuleRepository: FormuleRepository
	let handler: SouscrireAbonnementHandler

	beforeEach(() => {
		eventPublisher    = {
			publish: sinon.stub()
		}
		membreRepository  = {
			getTypePour: sinon.stub()
		}
		formuleRepository = {
			getPrix: sinon.stub().resolves(100),
		}
		handler           = new SouscrireAbonnementHandler(eventPublisher, membreRepository, formuleRepository)
	})

	it('La souscription mensuelle est réussie pour un étudiant', async () => {
		// GIVEN
		// @ts-ignore
		membreRepository.getTypePour.resolves(new TypeMembreEtudiant())
		const command = new SouscrireAbonnementCommand('formule-id', 'mensuelle', 'membre-id')

		// WHEN
		await handler.execute(command)

		// THEN
		expect(membreRepository.getTypePour).to.have.been.calledOnceWithExactly('membre-id')
		expect(formuleRepository.getPrix).to.have.been.calledOnceWithExactly('formule-id')
		expect(eventPublisher.publish).to.have.been.calledOnceWithExactly([new AbonnementSouscritParMembreEvent(
			'abonnement-id',
			'membre-id',
			'formule-id',
			80,
			true,
			'mensuelle')]
		)
	})

	it('La souscription mensuelle est réussie pour un membre standard', async () => {
		// GIVEN
		// @ts-ignore
		membreRepository.getTypePour.resolves(new TypeMembreStandard())
		const command = new SouscrireAbonnementCommand('formule-id', 'mensuelle', 'membre-id')

		// WHEN
		await handler.execute(command)

		// THEN
		expect(membreRepository.getTypePour).to.have.been.calledOnceWithExactly('membre-id')
		expect(formuleRepository.getPrix).to.have.been.calledOnceWithExactly('formule-id')
		expect(eventPublisher.publish).to.have.been.calledOnceWithExactly([new AbonnementSouscritParMembreEvent('abonnement-id',
			'membre-id',
			'formule-id',
			100,
			false,
			'mensuelle')]
		)
	})
})

