import chai, { expect }                     from 'chai'
import sinon                                from 'sinon'
import sinonChai                            from 'sinon-chai'
import { AbonnementSouscritParMembreEvent } from '../gestion-des-abonnements/domain/abonnement/abonnement-souscrit-par-membre-event'
import { NotificationSender }               from '../gestion-des-abonnements/domain/notification-sender'
import { AbonnementSouscritListener }       from '../gestion-des-abonnements/use-cases/abonnement-souscrit-listener'

chai.use(sinonChai)

describe('Abonnement souscrit listener', () => {
	it('Notification de la confirmation de la souscription à l\'abonnement', async () => {
		// GIVEN
		const notificationSender: NotificationSender = {
			send: sinon.stub()
		}
		const event = new AbonnementSouscritParMembreEvent('abonnement-id', 'membre-id', 'formule-id', 100, true, 'mensuelle')

		// WHEN
		await new AbonnementSouscritListener(notificationSender).handle(event)

		// THEN
		expect(notificationSender.send).to.have.been.calledOnceWithExactly('email@yopmail.com')
	})
})
