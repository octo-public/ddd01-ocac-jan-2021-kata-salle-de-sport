## Pierre, Papier et Ciseaux

Manches :
- Joueur Humain => Pierre, Papier et Ciseaux
- Joueur Machine => Aléatoirement (Pierre, Papier et Ciseaux)
-> Pierre > Ciseaux > Papier > Pierre
-> Au bout de 3 manches gagnées le joueur X gagne
  
=> Stocker le résultat de la manche quelque part

Récupérer tous les résultats de toutes les manches
